const { str,many,letters, digits,endOfInput,char,sequenceOf } = require('arcsecond');

const tag = type => value => ({  type, value });

const stringParser = str('hello').map(result=>({
  type: 'captured string',
  value: result
}));

const stringParserChain = many(str('hello')).map(results => results.map(x => x.toUpperCase()).join(''));

const stringParserSequence = sequenceOf([
  sequenceOf([letters,
             digits]).map(tag('letterDigits')),
             str('hello').map(tag('string')),
             many(char(' ')),
             str('world'),
             endOfInput
]);

console.log(
  stringParserSequence.run('dasdwqasdfkf12345hello          world')
);
